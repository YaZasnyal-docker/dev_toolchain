# Development toolchain

Based on centos:7

Contains:

* binutils - 2.36
* gdb - 12.1
* gcc - 8.3
* fpc - 3.2.2
* cmake - 3.27.5
* git - 2.37.0
* python - 3.8.9
* ninja - 1.11.0
* tarantool - 2.10.0
* icecc - 1.4
* ccache - 4.6.1

Libraries:
* boost - 1.79.0
* log4cplus - 1.2.x

# How to install:

## Manual
Download archive from the latest pipeline and unpack into ```/opt``` directory

## Script
Install latest dev_toolchain using script. ```Sudo``` before ```bash``` may be required to modify files in the ```/opt``` directory

```
wget -O - https://redirects.yazasnyal.dev/install_dev_toolchain.sh | bash
```
