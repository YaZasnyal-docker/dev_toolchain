#!/bin/bash

# Ensure root
if [ "$EUID" -ne 0 ]
    then echo "Please run as root"
    exit
fi

cd /opt
rm -rf dev_toolchain
wget -O - https://redirects.yazasnyal.dev/dev_toolchain.tar.gz | tar -xz
