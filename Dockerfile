ARG prefix=/opt/dev_toolchain

FROM centos:7 AS builder

# To build this container run command
# docker build . -t dev_container --build-arg arg=val ...
# 
# Arguments:
# Common:
# prefix - path that is passed to every component during configuration step (/opt/dev_toolchain by default)
# 
# DEPENDENCIES:
# ncurses_version - version of ncurses library
# readline_version - version of readline library
# icu_version - version of icu library
# unwind_version - version of libunwind library
# libffi_version - version of libffi library (for python)
# zlib_version - version of zlib library (for python)
# openssl_version - version of openssl library (for python)
# sqlite_version - version of sqlite library (for python)
# bzip2_version - version of bzip2 library (for python)
# lzo_version - version of lzo compression library (for icecc)
# capng_version - version of libcan-ng library (for icecc)
# archive_version - version of libarchive library (for icecc)
# zstd_version - version of Facebook's zstd compression library (for ccache)
# 
# Programs
# binutils_version - version of binutils (ls, add2line ...)
# gdb_version - version of GNU debugger
# gcc_version - version of gcc compiler
# fpc_version - version of freepascal compiler
# cmake_version - version of cmake
# git_version - version of git
# python_version - version of python interpreter
# ninja_version - version of ninja build system
# icecc_version - version of icecream distributed compilation cluster
# ccache_version - version of ccache utility (object file caching)
# 
# Libraries
# l4cpp_version - version of log4cplus library
# boost_version - version of boost libraries

ARG prefix
ARG private_prefix=${prefix}/usr/local
ARG prefix_path=${prefix}/lib:${prefix}/lib64:${private_prefix}/lib:${private_prefix}/lib64

# Installing build tools
RUN yum groupinstall -y "Development Tools" &&\
    yum install -y wget texinfo &&\
    yum install -y which curl zlib zlib-devel xz-devel openssl openssl-devel \
    gcc gcc-c++ git icu.x86_64 libicu-devel.x86_64 \
    asciidoc xmlto &&\
    yum update -y ca-certificates &&\
    yum clean all &&\
    rm -rf /var/cache/*

# Building gcc
ARG gcc_version=8.3.0
RUN cd /tmp && wget http://mirror.koddos.net/gcc/releases/gcc-${gcc_version}/gcc-${gcc_version}.tar.gz &&\
    tar -xf  gcc-${gcc_version}.tar.gz &&\
    cd gcc-${gcc_version} &&\
    ./contrib/download_prerequisites &&\
    ./configure --enable-languages=c,c++ --disable-multilib --prefix=${prefix} &&\
    make -j $(nproc) &&\
    make install-strip &&\
    cd / && rm -rf /tmp/*

ENV PATH=${prefix}/bin:${private_prefix}/bin:${PATH}
ENV CPATH=${prefix}/include:${private_prefix}/include:${CPATH}
ENV LD_LIBRARY_PATH=${prefix}/lib:${prefix}/lib64:${LD_LIBRARY_PATH}
ENV LIBRARY_PATH=${prefix_path}:${LD_LIBRARY_PATH}
ENV PKG_CONFIG_PATH=${prefix}/lib64/pkgconfig:${prefix}/lib/pkgconfig:${private_prefix}/lib64/pkgconfig:${private_prefix}/lib/pkgconfig
ENV LDFLAGS="-Wl,--disable-new-dtags -Wl,-rpath=${prefix_path}"
ENV CPPFLAGS="-fPIC"
ENV CFLAGS="-fPIC"
ENV CC=${prefix}/bin/gcc
ENV CXX=${prefix}/bin/g++

RUN yum install -y wget ncurses ncurses-devel

## DEPENDENCIES
# Building zlib
ARG zlib_version=1.3
RUN cd /tmp && wget https://zlib.net/zlib-${zlib_version}.tar.gz &&\
    tar -xvf zlib-${zlib_version}.tar.gz &&\
    cd zlib-${zlib_version} &&\
    ./configure --prefix=${private_prefix} &&\
    make -j$(nproc) &&\
    make install &&\
    cd / && rm -rf /tmp/*

# bzip2
ARG bzip2_version=1.0.8
RUN cd /tmp && wget https://sourceware.org/pub/bzip2/bzip2-${bzip2_version}.tar.gz &&\
    tar -xf bzip2-${bzip2_version}.tar.gz &&\
    cd bzip2-${bzip2_version} &&\
    sed -i -r 's/CFLAGS=(.*)/CFLAGS=-fPIC \1/g' Makefile &&\
    make &&\
    make install PREFIX=${private_prefix} &&\
    cd / && rm -rf /tmp/*

# openssl
ARG openssl_version=1.1.1q
RUN cd /tmp && wget https://ftp.openssl.org/source/openssl-${openssl_version}.tar.gz &&\
    tar xf openssl-${openssl_version}.tar.gz &&\
    cd openssl-${openssl_version} &&\
    ./config --prefix=${private_prefix} enable-egd &&\
    make -j$(nproc) &&\
    make install &&\
    cd / && rm -rf /tmp/*

# libffi
ARG libffi_version=3.4.2
RUN cd /tmp && git clone -b v${libffi_version} --depth=1 https://github.com/libffi/libffi.git &&\
    cd libffi &&\
    autoreconf -i &&\
    ./configure --prefix=${private_prefix} &&\
    make -j$(nproc) &&\
    make install &&\
    cd / && rm -rf /tmp/*

# curl
ARG curl_version=7.84.0
RUN cd /tmp && wget https://curl.se/download/curl-${curl_version}.tar.gz &&\
    tar xf curl-${curl_version}.tar.gz &&\
    cd curl-${curl_version}  &&\
    ./configure --prefix=${private_prefix} --with-openssl=${private_prefix} --with-zlib &&\
    make -j$(nproc) &&\
    make install &&\
    cd / && rm -rf /tmp/*

# ncurses
ARG ncurses_version=6.3
RUN cd /tmp && wget https://ftp.gnu.org/pub/gnu/ncurses/ncurses-${ncurses_version}.tar.gz &&\
    tar xf ncurses-${ncurses_version}.tar.gz &&\
    cd ncurses-${ncurses_version}  &&\
    ./configure --prefix=${private_prefix}\
                --enable-overwrite &&\
    make -j$(nproc) &&\
    make install &&\
    cd / && rm -rf /tmp/*

# readline
ARG readline_version=8.1.2
RUN cd /tmp && wget https://ftp.gnu.org/gnu/readline/readline-${readline_version}.tar.gz &&\
    tar xf readline-${readline_version}.tar.gz &&\
    cd readline-${readline_version}  &&\
    ./configure --prefix=${private_prefix} &&\
    make -j$(nproc) &&\
    make install &&\
    cd / && rm -rf /tmp/*

# unwind
ARG unwind_version=1.6.2
RUN cd /tmp && git clone -b v${unwind_version} --depth=1 https://github.com/libunwind/libunwind.git &&\
    cd libunwind &&\
    autoreconf -i &&\
    ./configure --prefix=${private_prefix} &&\
    make -j$(nproc) &&\
    make install &&\
    cd / && rm -rf /tmp/*

# icu
ARG icu_version=71-1
RUN cd /tmp && git clone -b release-${icu_version} --depth=1 https://github.com/unicode-org/icu.git &&\
    cd icu &&\
    mkdir icu4c-build && cd icu4c-build &&\
    ../icu4c/source/runConfigureICU Linux --prefix=${private_prefix} &&\
    make -j16 check &&\
    make install &&\
    cd / && rm -rf /tmp/*

## Main stuff
# Building binutils
ARG binutils_version=2.36
RUN cd /tmp &&\ 
    wget https://ftp.gnu.org/gnu/binutils/binutils-${binutils_version}.tar.xz &&\
    tar -xvf binutils-${binutils_version}.tar.xz &&\
    cd binutils-${binutils_version} &&\
    ./configure --prefix=${prefix} &&\
    make -j$(nproc) &&\
    make install &&\
    cd / && rm -rf /tmp/*

# Building python
ARG sqlite_version=3390000
ARG python_version=3.8.9
RUN cd /tmp && wget --no-check-certificate -c https://www.sqlite.org/2022/sqlite-autoconf-${sqlite_version}.tar.gz &&\
    tar xf sqlite-autoconf-${sqlite_version}.tar.gz &&\
    cd sqlite-autoconf-${sqlite_version} &&\
    ./configure --prefix=${private_prefix} &&\
    make -j$(nproc) &&\
    make install &&\
    \
    cd /tmp && wget https://www.python.org/ftp/python/${python_version}/Python-${python_version}.tgz &&\
    tar -xzf Python-${python_version}.tgz &&\
    cd Python-${python_version} &&\
    ./configure --prefix=${prefix} \
                --with-openssl=${private_prefix} \
                LDFLAGS="${LDFLAGS} -L${prefix}/lib -L${prefix}/lib64 -L${private_prefix}/lib -L${private_prefix}/lib64" \
                CPPFLAGS="-I${prefix}/include -I${private_prefix}/include" &&\
    make -j$(nproc) &&\
    make install &&\
    cd /tmp && yes | rm -R ./* 

# Building GNU debugger
ARG gdb_version=12.1
RUN yum install -y gmp-devel &&\
    yum clean all &&\
    rm -rf /var/cache/*
RUN cd /tmp &&\
    wget https://ftp.gnu.org/gnu/gdb/gdb-${gdb_version}.tar.xz &&\
    tar -xvf gdb-${gdb_version}.tar.xz &&\
    cd gdb-${gdb_version} &&\
    ./configure --prefix=${prefix} --with-python=${prefix}/bin/python3 &&\
    make -j$(nproc) &&\
    make install &&\
    cd / && rm -rf /tmp/*

# Building git
ARG git_version=2.42.0
RUN cd /tmp && git clone -b v${git_version} --depth=1 https://github.com/git/git.git &&\
    cd git &&\
    make configure &&\
    ./configure --prefix=${prefix} --with-zlib=${private_prefix} --with-python=${prefix} &&\
    make -j$(nproc) all doc &&\
    make install install-doc install-html &&\
    cd contrib/subtree && make -j$(nproc) && make prefix=${prefix} install &&\
    cd / && rm -rf /tmp/*

# Building cmake
ARG cmake_version=3.27.5
RUN cd /tmp && git clone -b v${cmake_version} --depth=1 https://gitlab.kitware.com/cmake/cmake.git &&\
    cd cmake &&\
    ./bootstrap --parallel=$(nproc) --prefix=${prefix} --verbose -- \
        -DCMAKE_BUILD_TYPE=RELEASE \
        -DCMAKE_PREFIX_PATH="${prefix_path};${private_prefix}" \
        -DOPENSSL_ROOT_DIR=${private_prefix} \
        &&\
    make -j$(nproc) &&\
    make install &&\
    cd / && rm -rf /tmp/*

# Building ninja
ARG ninja_version=1.11.0
RUN cd /tmp && git clone -b v${ninja_version} --depth=1 https://github.com/ninja-build/ninja.git &&\
    cd ninja &&\
    cmake -Bbuild-cmake -DCMAKE_INSTALL_PREFIX=${prefix} -H. &&\
    cmake --build build-cmake &&\
    cmake --install build-cmake &&\
    cd / && rm -rf /tmp/*

# Building log4cplus
ARG log4cplus_version=1.2.x
RUN cd /tmp && git clone -b ${log4cplus_version} --depth=1 https://github.com/log4cplus/log4cplus.git &&\
    cd log4cplus &&\
    mkdir build && cd build &&\
    cmake .. -DCMAKE_INSTALL_PREFIX=${prefix} &&\
    make -j$(nproc) &&\
    make install &&\
    cd / && rm -rf /tmp/*

# Building boost
ARG boost_version=1.80.0
RUN cd /tmp &&\
    export BOOSTMAJOR=$(echo ${boost_version} | sed -r "s/([0-9]+)\.([0-9]+)\.([0-9]+)$/\1/") &&\
    export BOOSTMINOR=$(echo ${boost_version} | sed -r "s/([0-9]+)\.([0-9]+)\.([0-9]+)$/\2/") &&\
    export BOOSTHOTFIX=$(echo ${boost_version} | sed -r "s/([0-9]+)\.([0-9]+)\.([0-9]+)$/\3/") &&\
	wget https://boostorg.jfrog.io/artifactory/main/release/${BOOSTMAJOR}.${BOOSTMINOR}.${BOOSTHOTFIX}/source/boost_${BOOSTMAJOR}_${BOOSTMINOR}_${BOOSTHOTFIX}.tar.gz &&\
	tar -xf boost_${BOOSTMAJOR}_${BOOSTMINOR}_${BOOSTHOTFIX}.tar.gz &&\
	cd boost_${BOOSTMAJOR}_${BOOSTMINOR}_${BOOSTHOTFIX} &&\
	./bootstrap.sh &&\
	./b2 --without-python link=static -j $(nproc) cxxflags=-fPIC cflags=-fPIC install --prefix=${prefix} &&\
    cd /tmp && yes | rm -R ./*

# Install freepascal
ARG fpc_version=3.2.2
RUN cd /tmp &&\
    wget -O fpc-${fpc_version}.x86_64-linux.tar https://sourceforge.net/projects/freepascal/files/Linux/${fpc_version}/fpc-${fpc_version}.x86_64-linux.tar/download &&\
    tar -xf fpc-${fpc_version}.x86_64-linux.tar &&\
    cd fpc-${fpc_version}.x86_64-linux &&\
    echo "/opt/dev_toolchain" | ./install.sh &&\
    cd /tmp && yes | rm -R ./*

# (DEPENDENCY) zstd
ARG zstd_version=1.5.2
RUN cd /tmp && git clone -b v${zstd_version} --depth=1 https://github.com/facebook/zstd.git &&\
    cd zstd/build/cmake && mkdir build && cd build &&\
    cmake ../ -DCMAKE_INSTALL_PREFIX=${private_prefix} &&\
    make -j$(nproc) &&\
    make install &&\
    cd / && rm -rf /tmp/*

# (DEPENDENCY) lzo2
ARG lzo_version=2.10
RUN cd /tmp && wget http://www.oberhumer.com/opensource/lzo/download/lzo-${lzo_version}.tar.gz &&\
    tar -xf lzo-${lzo_version}.tar.gz &&\
    cd lzo-${lzo_version} &&\
    ./configure --prefix=${private_prefix} &&\
    make -j$(nproc) &&\
    make install &&\
    cd / && rm -rf /tmp/*

# (DEPENDENCY) cap-ng
ARG capng_version=0.8.3
RUN cd /tmp && git clone -b v${capng_version} --depth=1 https://github.com/stevegrubb/libcap-ng.git &&\
    cd libcap-ng &&\
    ./autogen.sh &&\
    ./configure --prefix=${private_prefix} &&\
    make -j$(nproc) &&\
    make install &&\
    cd / && rm -rf /tmp/*

# (DEPENDENCY) libarchive
ARG archive_version=3.6.1
RUN cd /tmp && git clone -b v${archive_version} --depth=1 https://github.com/libarchive/libarchive.git &&\
    cd libarchive && mkdir .build && cd .build &&\
    cmake ../ -DCMAKE_INSTALL_PREFIX=${private_prefix} -DENABLE_OPENSSL=OFF -DENABLE_CNG=OFF -DENABLE_LZO=ON &&\
    make -j$(nproc) &&\
    make install &&\
    cd / && rm -rf /tmp/*

# icecc
ARG icecc_version=1.4
RUN cd /tmp &&\
    git clone -b ${icecc_version} --depth=1 https://github.com/icecc/icecream.git &&\
    cd icecream && ./autogen.sh &&\
    ./configure --prefix=${prefix} --without-man &&\
    make -j$(nproc) &&\
    make install &&\
    cp daemon/iceccd ${prefix}/bin &&\
    cp scheduler/icecc-scheduler ${prefix}/bin &&\
    cd / && rm -rf /tmp/*

# ccache
ARG ccache_version=4.6.1
RUN cd /tmp &&\
    git clone -b v${ccache_version} --depth=1 https://github.com/ccache/ccache.git &&\
    cd ccache &&\
    mkdir build && cd build &&\
    cmake ../ \
        -DREDIS_STORAGE_BACKEND=OFF \
        -DCMAKE_SYSTEM_PREFIX_PATH="${prefix};${private_prefix}" \
        -DCMAKE_INSTALL_PREFIX=${prefix} &&\
    make -j$(nproc) &&\
    make install &&\
    cd / && rm -rf /tmp/*

# Activation script
# Set environment variables
RUN PYTHONVERS=$(echo ${python_version} | sed -r "s/([0-9]+)\.([0-9]+)\.([0-9]+)$/\1.\2/") && echo -e \
"export PATH=${prefix}/bin:\${PATH}\n\
export CC=${prefix}/bin/gcc\n\
export CXX=${prefix}/bin/g++\n\
export MANPATH=${prefix}/share/man:\${MANPATH}\n\
export INFOPATH=${prefix}/share/info:\${INFOPATH}\n\
export CPATH=${prefix}/include:\${CPATH}\n\
export LIBRARY_PATH=${prefix}/lib64:${prefix}/lib:\${LIBRARY_PATH} \n\
export LD_LIBRARY_PATH=${prefix}/lib64:${prefix}/lib:\${LD_LIBRARY_PATH}\n\
# export PYTHONPATH=${prefix}/lib/python${PYTHONVERS}/site-packages:${prefix}/lib64/python${PYTHONVERS}/site-packages/:${PYTHONPATH}\n\
export PKG_CONFIG_PATH=${prefix}/lib64/pkgconfig:${prefix}/lib/pkgconfig:\${PKG_CONFIG_PATH}\n\
" > ${prefix}/enable &&\
chmod +x ${prefix}/enable

FROM centos:7
ARG prefix
COPY --from=builder ${prefix} ${prefix}
RUN yum install -y \
    flex strace strace64 make pkgconfig gettext automake bison libtool autoconf which wget curl &&\
    yum update -y ca-certificates &&\
    yum clean all &&\
    rm -rf /var/cache/*
ENV PATH=${prefix}/bin:${PATH}
ENV MANPATH=${prefix}/share/man:${MANPATH}
ENV INFOPATH=${prefix}/share/info:${INFOPATH}
ENV CPATH=${prefix}/include:${CPATH}
ENV LIBRARY_PATH=${prefix}/lib64:${prefix}/lib:${LIBRARY_PATH}
ENV LD_LIBRARY_PATH=${prefix}/lib64:${prefix}/lib:${LD_LIBRARY_PATH}
ENV PKG_CONFIG_PATH=${prefix}/lib64/pkgconfig:${prefix}/lib/pkgconfig:${PKG_CONFIG_PATH}
